# Internet VPC
resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  instance_tenancy     = "default"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"

  tags {
    Name = "k8-vpc"
  }
}

# Public Subnets
resource "aws_subnet" "k8-vpc-public-1" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-west-2a"

  tags {
    Name = "k8-vpc-public-subnet-1"
  }
}

resource "aws_subnet" "k8-vpc-public-2" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-west-2b"

  tags {
    Name = "k8-vpc-public-subnet-2"
  }
}

resource "aws_subnet" "k8-vpc-public-3" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-west-2c"

  tags {
    Name = "k8-vpc-public-subnet-3"
  }
}

# Private Subnets
resource "aws_subnet" "k8-vpc-private-1" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "10.0.4.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-west-2a"

  tags {
    Name = "k8-vpc-private-subnet-1"
  }
}

resource "aws_subnet" "k8-vpc-private-2" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "10.0.5.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-west-2b"

  tags {
    Name = "k8-vpc-private-subnet-2"
  }
}

resource "aws_subnet" "k8-vpc-private-3" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "10.0.6.0/24"
  map_public_ip_on_launch = "false"
  availability_zone       = "us-west-2c"

  tags {
    Name = "k8-vpc-private-subnet-3"
  }
}

# Internet GW
resource "aws_internet_gateway" "k8-vpc-gw" {
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "k8-vpc"
  }
}

# Public route table
resource "aws_route_table" "k8-vpc-routetable-public" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.k8-vpc-gw.id}"
  }

  tags {
    Name = "k8-vpc-routetable-public"
  }
}

# Public route table associations 
resource "aws_route_table_association" "k8-vpc-public-1-association" {
  subnet_id      = "${aws_subnet.k8-vpc-public-1.id}"
  route_table_id = "${aws_route_table.k8-vpc-routetable-public.id}"
}

resource "aws_route_table_association" "k8-vpc-public-2-association" {
  subnet_id      = "${aws_subnet.k8-vpc-public-2.id}"
  route_table_id = "${aws_route_table.k8-vpc-routetable-public.id}"
}

resource "aws_route_table_association" "k8-vpc-public-3-association" {
  subnet_id      = "${aws_subnet.k8-vpc-public-3.id}"
  route_table_id = "${aws_route_table.k8-vpc-routetable-public.id}"
}
