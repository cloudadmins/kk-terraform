variable "AWS_REGION" {
  default = "us-west-2"
}

# variable "PATH_TO_PRIVATE_KEY" {
#   default = "mykey"
# }
# variable "PATH_TO_PUBLIC_KEY" {
#   default = "mykey.pub"
# }

variable "ami" {
  default = "ami-0d1cd67c26f5fca19"
}

variable "AMIS" {
  type = "map"

  default = {
    us-east-1 = "ami-13be557e"
    us-west-2 = "ami-00e3742249efe2844"
    eu-west-1 = "ami-0d729a60"
  }
}
