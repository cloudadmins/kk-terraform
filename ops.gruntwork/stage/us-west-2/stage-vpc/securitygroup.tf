resource "aws_security_group" "stage-security-group-1" {
  vpc_id      = "${aws_vpc.main.id}"
  name        = "k8-vpc-stage-sg-all-traffic"
  description = "security group that allows ssh and all egress traffic"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow-all"
  }
}
