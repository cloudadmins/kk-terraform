# nat gw
resource "aws_eip" "k8-vpc-nat" {
  vpc = true
}

resource "aws_nat_gateway" "k8-vpc-nat-gw" {
  allocation_id = "${aws_eip.k8-vpc-nat.id}"
  subnet_id     = "${aws_subnet.k8-vpc-public-1.id}"
  depends_on    = ["aws_internet_gateway.k8-vpc-gw"]
}

# VPC setup for NAT
resource "aws_route_table" "k8-vpc-routetable-private" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.k8-vpc-nat-gw.id}"
  }

  tags {
    Name = "k8-vpc-routetable-private"
  }
}

# Private route table associations
resource "aws_route_table_association" "k8-vpc-private-1-association" {
  subnet_id      = "${aws_subnet.k8-vpc-private-1.id}"
  route_table_id = "${aws_route_table.k8-vpc-routetable-private.id}"
}

resource "aws_route_table_association" "k8-vpc-private-2-association" {
  subnet_id      = "${aws_subnet.k8-vpc-private-2.id}"
  route_table_id = "${aws_route_table.k8-vpc-routetable-private.id}"
}

resource "aws_route_table_association" "k8-vpc-private-3-association" {
  subnet_id      = "${aws_subnet.k8-vpc-private-3.id}"
  route_table_id = "${aws_route_table.k8-vpc-routetable-private.id}"
}
