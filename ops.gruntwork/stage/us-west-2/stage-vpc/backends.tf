terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket = "kk-terraform-backends-stage"
    key    = "us-west-2/stage/vpc.tfstate"
    region = "us-west-2"

    # Replace this with your DynamoDB table name!
    dynamodb_table = "terraform-backends-stage"
    encrypt        = true
  }
}
