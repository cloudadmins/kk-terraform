resource "aws_key_pair" "k8-vpc-stage-key" {
  key_name = "k8vpc-stage-keypair"

  # Filename: /Users/karthikeyan/.ssh/aws002.pub
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCweCdC0uNkikCFxKcAbLc0OHOnTNsNf9Krf91VLX3sbFOnDpEjrQ6LdloFW1CR+/c2ZtnxnCvXxxsWo5qrQ6+SWCbiAxBys6eC2SDbdCPZfg5VMa+SXZcyddrb+JqozNCqep14FQc36nGuhefQp3xErbIK4lvbNT5hzlGJXdl9XR8imz5E5xFRSmbTUND0ldi6K/5U3zjFS0RjTPat07fUvyNND/gyhlOVo6jHgRHj8O7l0YXACwJZYMjLnMufp+upL8rYuV2WhWF8lBmMQFxT+mY6XTLrRYcfYpsrSKZdp2+rAIpSccv1+rsCywVxApq2+kr9CvmyDbFrFCpds5on karthikeyan@BLDM3198-MAC.local"
}
