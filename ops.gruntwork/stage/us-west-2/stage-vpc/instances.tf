resource "aws_instance" "k8-master" {
  # ami               = "${lookup(var.AMIS, var.AWS_REGION)}"
  ami           = "ami-0d1cd67c26f5fca19"
  instance_type = "c5.large"

  # the VPC subnet
  subnet_id = "${aws_subnet.k8-vpc-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.stage-security-group-1.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.k8-vpc-stage-key.key_name}"

  tags {
    Name = "k8-master-node"
  }
}

resource "aws_instance" "k8-workernode-1" {
  # ami               = "${lookup(var.AMIS, var.AWS_REGION)}"
  ami           = "ami-0d1cd67c26f5fca19"
  instance_type = "c5.large"

  # the VPC subnet
  subnet_id = "${aws_subnet.k8-vpc-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.stage-security-group-1.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.k8-vpc-stage-key.key_name}"

  tags {
    Name = "k8-workernode-1"
  }
}

resource "aws_instance" "k8-workernode-2" {
  # ami               = "${lookup(var.AMIS, var.AWS_REGION)}"
  ami           = "ami-0d1cd67c26f5fca19"
  instance_type = "c5.large"

  # the VPC subnet
  subnet_id = "${aws_subnet.k8-vpc-public-1.id}"

  # the security group
  vpc_security_group_ids = ["${aws_security_group.stage-security-group-1.id}"]

  # the public SSH key
  key_name = "${aws_key_pair.k8-vpc-stage-key.key_name}"

  tags {
    Name = "k8-workernode-2"
  }
}
